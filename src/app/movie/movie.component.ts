import { Component, OnInit, Input,  Output, EventEmitter  } from '@angular/core';

@Component({
  selector: 'movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {
  @Input() data:any;
  @Output() deleteMe = new EventEmitter<any>();

  id;
  title;
  studio;
  weekendIncom;
  //show = true;

  constructor() {


   }

  ngOnInit() {
    this.title = this.data.title;
    this.id= this.data.id;
    this.weekendIncom= this.data.weekendIncom;
    this.studio= this.data.studio;
  }

  
  //deleteRow() {
  //  this.show = false;
  //}

}
